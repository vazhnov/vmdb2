# Copyright 2022  Lars Wirzenius
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=


import logging
import os
import shutil
import tempfile

import vmdb


class CryptsetupPlugin(vmdb.Plugin):
    def enable(self):
        self.app.step_runners.add(CryptsetupStepRunner())


class CryptsetupStepRunner(vmdb.StepRunnerInterface):
    def get_key_spec(self):
        return {
            "cryptsetup": str,
            "name": str,
            "password": "",
            "key-file": "",
            "key-cmd": "",
        }

    def run(self, step, settings, state):
        underlying = step["cryptsetup"]
        crypt_name = step["name"]
        password = step["password"] or None
        key_file = step["key-file"] or None
        key_cmd = step["key-cmd"] or None

        if not isinstance(underlying, str):
            raise vmdb.NotString("cryptsetup", underlying)

        if not isinstance(crypt_name, str):
            raise vmdb.NotString("cryptsetup: tag", crypt_name)

        if password is None and key_file is None and key_cmd is None:
            raise Exception(
                "cryptsetup step MUST define one of password, key-file, or key-cmd"
            )

        if password is not None and key_file is not None:
            raise Exception(
                "cryptsetup step MUST define only one of password or key-file"
            )

        if password is not None and key_cmd is not None:
            raise Exception(
                "cryptsetup step MUST define only one of password or key-cmd"
            )

        if key_file is not None and key_cmd is not None:
            raise Exception(
                "cryptsetup step MUST define only one of key_file or key-cmd"
            )

        state.tmp_key_file = None
        rmtmp = False

        if password is not None:
            key_file = self._write_temp(password)
            rmtmp = True

        if key_cmd is not None:
            output = vmdb.runcmd(["sh", "-ec", key_cmd])
            output = output.decode("UTF-8")
            key = output.splitlines()[0]
            key_file = self._write_temp(key)
            rmtmp = True

        assert key_file is not None

        dev = state.tags.get_dev(underlying)
        if dev is None:
            for t in state.tags.get_tags():
                logging.debug(
                    "tag %r dev %r mp %r",
                    t,
                    state.tags.get_dev(t),
                    state.tags.get_builder_mount_point(t),
                )
            assert 0

        vmdb.runcmd(
            [
                "cryptsetup",
                "luksFormat",
                "--batch-mode",
                "--type=luks2",
                "--pbkdf=argon2id",
                dev,
                key_file,
            ]
        )
        vmdb.runcmd(
            [
                "cryptsetup",
                "open",
                "--key-file",
                key_file,
                "--allow-discards",
                dev,
                crypt_name,
            ]
        )

        crypt_dev = "/dev/mapper/{}".format(crypt_name)
        assert os.path.exists(crypt_dev)

        uuid = vmdb.runcmd(["cryptsetup", "luksUUID", dev]).decode("UTF8").strip()

        state.tags.append(crypt_name)
        state.tags.set_dev(crypt_name, crypt_dev)
        state.tags.set_luksuuid(crypt_name, uuid)
        state.tags.set_dm(crypt_name, crypt_name)

        vmdb.progress(
            f"LUKS: name={crypt_name} dev={crypt_dev} luksuuid={uuid} dm={crypt_name}"
        )
        vmdb.progress(f"LUKS: {state.tags._tags}")
        vmdb.progress("remembering LUKS device {} as {}".format(crypt_dev, crypt_name))

        if rmtmp:
            os.remove(key_file)

    def _write_temp(self, password):
        fd, filename = tempfile.mkstemp()
        os.close(fd)
        open(filename, "w").write(password)
        return filename

    def teardown(self, step, settings, state):
        x = state.tmp_key_file
        if x is not None and os.path.exists(x):
            os.remove(x)

        crypt_name = step["name"]

        crypt_dev = "/dev/mapper/{}".format(crypt_name)
        vmdb.runcmd(["cryptsetup", "close", crypt_dev])
